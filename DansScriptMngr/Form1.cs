﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DansScriptMngr
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ShowUserControl("sop");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ShowUserControl("img");
        }

        public void ShowUserControl(string val)
        {
            UserControl uc = new UserControl();
            pnlMain.Controls.Clear();

            if (val == "sop")
            {
                uc = new ctlSop();
            }

            if (val == "img")
            {
                uc = new ctlImgBuilder();
            }

            pnlMain.Controls.Add(uc);
        }
    }
}
